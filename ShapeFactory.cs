using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareAssignment
{
    /// <summary>
    /// Shape factory stores an instance of shape. Here we can create random shapes by pulling them from here
    /// </summary>
    class ShapeFactory
    {

        public Shape getShape(String shapeType)
        {
            shapeType = shapeType.ToLower().Trim();


            if (shapeType.Equals("circle"))
            {
                return new Circle();

            }
            else if (shapeType.Equals("rectangle"))
            {
                return new Rectangle();

            }
            else if (shapeType.Equals("triangle"))
            {
                return new Triangle();
            }
            else
            {
                //if command entered does not equal one of the shapes above then an error is thrown
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + " does not exist");
                throw argEx;
            }


        }
    }
}
