﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareAssignment
{
    /// <summary>
    /// The rectangle inherits the x,y but also has a width and height which are unique to the Rectangle
    /// </summary>
    public class Rectangle : Shape
    {
        int width, height;

        public Rectangle():base()
        {
            width = height = 100;
        }

        public Rectangle(int width, int height)
        {
            this.width = width;
            this.height = height;
        }
        //The structure in which the rectangle will be created within the form
        public Rectangle(Color colour ,int x, int y, int width, int height) : base(colour,x, y)
        {
            this.width = width;
            this.height = height;
        }

        public override void setShape(Color colour, params int[] list)
        {
            //colour,x,y are all inherited from the base class and width list[2] height list [3] are unique to rectangle
            base.setShape(colour, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];

        }

        //A pen and brush are declared specific for the rectangle when this is called and disposed of once drawn
        public override void drawShape(Graphics g)
        {
            Pen p = new Pen(Color.Black, 2);
            Brush b = new SolidBrush(colour);
            g.FillRectangle(b, x, y, width, height);
            g.DrawRectangle(p, x, y, width, height);
            p.Dispose();
            b.Dispose();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
