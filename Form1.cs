using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SoftwareAssignment
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Global declarations for the program are stored up here to be referenced throughout the program
        /// </summary>
        private ArrayList programLine = new ArrayList();
        private String[] variableNames = new String[50];
        private int[] variableValues = new int[50];
        private int x, y, count, radius, width, height, s1, s2, s3, p1, p2, loopIt, loopCounter, loopSize, variableCounter,ifVar,ifValue,ifSize;
        private Color colour = Color.Green;
        ShapeFactory factory = new ShapeFactory();
        Random rand = new Random(77887);
        bool loopFlag , executeLinesFlag = false;
        BindingList<string> errors = new BindingList<string>();
        public Form1()  
        {
            InitializeComponent();
            Invalidate();


        }
        /// <summary>
        /// Random number generator used for the factory case as all shapes share a position and a colour so it makes for less repeating
        /// code by just bring it in up here instead and calling it into the switch area for all the shapes to use when the factory case
        /// is used
        /// </summary>

        public void randomGen()
        {

            x = rand.Next(pictureBox1.Width);
            y = rand.Next(pictureBox1.Height);

            int red = rand.Next(255);
            int green = rand.Next(255);
            int blue = rand.Next(255);

            colour = Color.FromArgb(255, red, green, blue);

        }




        /// <summary>
        /// The commandParser method is used to determine which input is being used within the program.
        /// A run command within the textbox results in the method counting the lines within the richtextbox and 
        /// submitting them to be displayed as the relative commands within the picturebox.
        /// </summary>
        public void commandParser()
        {
            if (textBox1.Text.ToLower() == "run")
            {   //ignore empty lines and splitting text when the line is changed
                String[] lines = richTextBox1.Text.Split("\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                String[] line;
                int i = 0; 

                    while (lines.Length != i)
                    {
                        
                        line = lines[i].Split(' ');//splitting by spaces within each line
                        programLine.Add(line);
                        i++;
                    }
                    if (lines.Length != i)
                    {
                    MessageBox.Show("Program empty unable to run", "Error");
                }
                
            }
            else
            {
                String[] lines = textBox1.Lines.ToArray();//textbox only has one line so doesn't need to worry about split yet
                String[] line;
                int i = 0;
                while (lines.Length != i)
                {
                    line = lines[i].Split(' ');
                    programLine.Add(line);
                    i++;
                  
                }
                if (lines.Length == 0)
                {
                    MessageBox.Show("Program empty unable to run", "Error");
                }
            }
        }

        /// <summary>
        /// We use this method to check if a variable is found within the variableNames array by comparing the param against
        /// the input in the variables case. The method will loop++ through the array until it has read every entry
        /// if it could not find the value then the variable can be declared as new else it will return -1 and the user will be notified
        /// that the variable already exists
        /// </summary>
        private int checkVariable(string param)
        {
            for (int loop = 0; loop < variableCounter; loop++) {
                if (variableNames[loop] == param) {
                    return loop++;
                }
            }
            return -1;
            throw new NotImplementedException();
        }
        /// <summary>
        /// This method is used to read the variable value associated with the variable name that is given at the same index
        /// we declare a new int called number so we can initialize it to be equal to the value of the variable then return the
        /// number in place of the value of the command name typed in
        /// </summary>
        private int readVar(string variable) {

            
            int found = checkVariable(variable);
            if (found >= 0)
            {
                variable = variableValues[found].ToString();
                int number = int.Parse(variable);
                return number;
            }
            else
            {
                MessageBox.Show("Invalid command");
            }

            return -1;


        }



        /// <summary>
        /// This is the main event for processing the input through the form.
        /// The keydown event is used so the user can press the enter key to enter a command once they have wrote it.
        /// </summary>
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {


            //The graphics and the pen are declared in here so they can be used within the switch of this event
            Graphics g = pictureBox1.CreateGraphics();

            if (e.KeyCode == Keys.Enter)
            {

                {
                    //Here we call the method into the event where it will choose which split to use based on the input


                    commandParser();

                    try
                    {



                        //Runs everything within the switch for as long as the programLine.Count is greater than the count
                        while (programLine.Count > count)
                        {


                            if (loopFlag == true)
                            {
                                loopSize++;
                            }

                            if (executeLinesFlag == true)
                            {

                                ifSize++;
                            }

                            //the part array holds each section of the current line
                            String[] part = (String[])programLine[count++];
                            Shape shapeDefault;
                            switch (part[0].ToLower())//starting at part[0] which is the case value .toLower accepts lowercase and uppercase
                            {

                                //default case if part[0] is not equal to a case then this is displayed instead
                                default:
                                    string variable = part[0];
                                    if (variable == part[0])
                                    {
                                        checkVariable(variable);
                                        readVar(variable);

                                    }
                                    else
                                    {
                                        MessageBox.Show("invalid command", "Error");
                                    }
                                    break;

                                //clear sets the picturebox to null clearing all graphics on the screen
                                case "clear":
                                    pictureBox1.Image = null;
                                    MessageBox.Show("The drawing area has been cleared", "Successful");
                                    break;

                                //reset sets x,y back to their original values of 0 (top left of picturebox)
                                case "reset":
                                    this.x = 0;
                                    this.y = 0;
                                    MessageBox.Show("The pen position has been reset", "Successful");
                                    break;

                                //save sends everything currently within the richtextbox to be stored in a txt file
                                case "save":
                                    richTextBox1.SaveFile(@"\Program.txt", RichTextBoxStreamType.PlainText);
                                    MessageBox.Show("The current program has been saved", "Successful");
                                    break;

                                //load brings the previous saved file back into the richtextbox
                                case "load":
                                    richTextBox1.LoadFile(@"\Program.txt", RichTextBoxStreamType.PlainText);
                                    MessageBox.Show("The current program has been loaded", "Successful");
                                    break;

                                case "colour":
                                    if (part[1] == "")//checks if the values given are null if so error
                                    {
                                        MessageBox.Show("Cannot return null must have a value", "Error");
                                    }
                                    else
                                    {

                                        colour = (Color.FromName(part[1]));
                                    }
                                    break;

                                //moveto positions the pen where specified
                                case "moveto":
                                    if (part[1] == "" || part[2] == "")//checks if the values given are null if so error
                                    {
                                        MessageBox.Show("Cannot return null must have a value", "Error");
                                    }
                                    else//if the value cannot be passed as an integer in each section then it will error
                                    {
                                        if (!int.TryParse(part[1], out x) || !int.TryParse(part[2], out y))
                                        {
                                            this.x = readVar(part[1]);
                                            this.y = readVar(part[2]);
                                        }
                                        else//else the command is carried out
                                        {
                                            this.x = int.Parse(part[1]);
                                            this.y = int.Parse(part[2]);

                                        }
                                    }
                                    break;

                                //drawto draws a line from the x position to the y position
                                case "drawto":
                                    if (part[1] == "" || part[2] == "")
                                    {
                                        MessageBox.Show("Cannot return null must have a value", "Error");
                                    }
                                    else
                                    {
                                        if (!int.TryParse(part[1], out p1) || !int.TryParse(part[2], out p2))
                                        {
                                            p1 = readVar(part[1]);
                                            p2 = readVar(part[2]);
                                            Point pt1 = new Point(p1, p2);//points must be connected for the line to draw
                                            Point pt2 = new Point(p2, p1);
                                            Pen pen = new Pen(colour, 3);
                                            g.DrawLine(pen, pt1, pt2);//references the pen within the event and the points
                                            pen.Dispose();//pen is disposed once finished

                                        }
                                        else
                                        {
                                            int.Parse(part[1]);
                                            int.Parse(part[2]);
                                            Point pt1 = new Point(p1, p2);//points must be connected for the line to draw
                                            Point pt2 = new Point(p2, p1);
                                            Pen pen = new Pen(colour, 3);
                                            g.DrawLine(pen, pt1, pt2);//references the pen within the event and the points
                                            pen.Dispose();//pen is disposed once finished

                                        }
                                    }
                                    break;

                                //A circle is drawn based on the radius
                                case "circle":
                                    if (part[1] == "")
                                    {
                                        MessageBox.Show("Cannot return null must have a value", "Error");
                                    }
                                    else
                                    {
                                        if (!int.TryParse(part[1], out radius))
                                        {
                                            radius = readVar(part[1]);
                                            new Circle(colour, x, y, radius).drawShape(g);//this references the circle class
                                        }
                                        else
                                        {
                                            int.Parse(part[1]);
                                            new Circle(colour, x, y, radius).drawShape(g);//this references the circle class

                                        }
                                    }
                                    break;

                                //A rectangle is drawn based on the width and height specified
                                case "rectangle":
                                    if (part[1] == "" || part[2] == "")
                                    {
                                        MessageBox.Show("Cannot return null must have a value", "Error");

                                    }
                                    else
                                    {
                                        if (!int.TryParse(part[1], out width) || !int.TryParse(part[2], out height))
                                        {
                                            width = readVar(part[1]);
                                            height = readVar(part[2]);
                                            new Rectangle(colour, x, y, width, height).drawShape(g);//references to the Rectangle class
                                        }
                                        else
                                        {
                                            int.Parse(part[1]);
                                            int.Parse(part[2]);
                                            new Rectangle(colour, x, y, width, height).drawShape(g);//references to the Rectangle class

                                        }

                                    }
                                    break;

                                //A triangle is drawn based on the specified points
                                case "triangle":
                                    if (part[1] == "" || part[2] == "" || part[3] == "")
                                    {
                                        MessageBox.Show("Cannot return null must have a value", "Error");

                                    }
                                    else
                                    {

                                        if (!int.TryParse(part[1], out s1) || !int.TryParse(part[2], out s2) || !int.TryParse(part[3], out s3))
                                        {
                                            s1 = readVar(part[1]);
                                            s2 = readVar(part[2]);
                                            s3 = readVar(part[3]);
                                            Point sa = new Point(s1, s2);//Points need to be connected to each other
                                            Point sb = new Point(s2, s3);
                                            Point sc = new Point(s3, s1);
                                            Point[] sides = { sa, sb, sc };//All combinations are put into the point array
                                            new Triangle(colour, x, y, sides).drawShape(g);//references the triangle class
                                        }
                                        else
                                        {
                                            int.Parse(part[1]);
                                            int.Parse(part[2]);
                                            int.Parse(part[3]);
                                            Point sa = new Point(s1, s2);//Points need to be connected to each other
                                            Point sb = new Point(s2, s3);
                                            Point sc = new Point(s3, s1);
                                            Point[] sides = { sa, sb, sc };//All combinations are put into the point array
                                            new Triangle(colour, x, y, sides).drawShape(g);//references the triangle class

                                        }

                                    }
                                    break;
                                //factory class to generate shapes
                                case "factory":

                                    if (part[1].ToLower().Equals("circle"))
                                    {
                                        radius = rand.Next(50, 150);
                                        randomGen();
                                        shapeDefault = factory.getShape("circle");
                                        shapeDefault.setShape(colour, x, y, radius);
                                        shapeDefault.drawShape(g);
                                    }
                                    else if (part[1].ToLower().Equals("rectangle"))
                                    {
                                        width = rand.Next(150, 200);
                                        height = rand.Next(50, 100);
                                        randomGen();
                                        shapeDefault = factory.getShape("rectangle");
                                        shapeDefault.setShape(colour, x, y, width, height);
                                        shapeDefault.drawShape(g);
                                    }
                                    else if (part[1].ToLower().Equals("triangle"))
                                    {
                                        s1 = rand.Next(pictureBox1.Height);
                                        s2 = rand.Next(pictureBox1.Width);
                                        s3 = rand.Next(pictureBox1.Width);
                                        randomGen();
                                        Point sa = new Point(s1, s2);//Points need to be connected to each other
                                        Point sb = new Point(s2, s3);
                                        Point sc = new Point(s3, s1);
                                        Point[] sides = { sa, sb, sc };//All combinations are put into the point array
                                        shapeDefault = factory.getShape("triangle");
                                        shapeDefault.setTriangle(colour, x, y, sides);
                                        shapeDefault.drawShape(g);
                                    }
                                    else
                                    {
                                        System.ArgumentException argEx = new System.ArgumentException("Factory error: " + part[1] + " does not exist");
                                        throw argEx;
                                    }

                                    break;

                                case "loop":
                                    loopFlag = true; //If the line is still within a loop
                                    loopCounter = 0;//The amount of times the loop has been round
                                    loopSize = 0;// The amount of text within a loop

                                    if (part[1] == "")//checks if the values given are null if so error
                                    {
                                        loopFlag = false; //If the line is still within a loop
                                        MessageBox.Show("Cannot return null must have a value", "Error");

                                    }
                                    else//if the value cannot be passed as an integer in each section then it will error
                                    {
                                        if (!int.TryParse(part[1], out loopIt))
                                        {
                                            loopIt = readVar(part[1]);
                                        }
                                        else//else the command is carried out
                                        {
                                            loopIt = int.Parse(part[1]); //How many times to run the loop
                                        }
                                    }
                                    break;

                                case "endloop":

                                    loopFlag = false;
                                    loopCounter++;

                                    if (loopCounter < loopIt)
                                    {

                                        count = count - loopSize;
                                    }


                                    break;



                                case "var":

                                    //declare variables initialize it first here
                                    if (part.Length == 2)
                                    {
                                        string param = part[1];
                                        int found = checkVariable(param);
                                        if (found >= 0)
                                        {
                                            MessageBox.Show("command already exists", "Error");

                                        }
                                        else
                                        {
                                            variableNames[variableCounter] = param;
                                            variableValues[variableCounter++] = 0;

                                        }
                                    }

                                    //if the var equals this then it knows a value is being assigned not created
                                    else if (part.Length == 4)

                                    {
                                        if (part[2] == "=")
                                        {
                                            string param = part[1];
                                            string command = part[3];
                                            int found = checkVariable(param);
                                            if (found >= 0)
                                            {

                                                variableNames[found] = param;
                                                variableValues[found] = int.Parse(command);
                                                MessageBox.Show(param + command);

                                            }
                                            else
                                            {
                                                MessageBox.Show("Incorrect Syntax");
                                            }

                                        }
                                    }



                                    break;


                                //if statement if true complte lines of code between the if and endif else skip the lines
                                case "if":
                                    ifVar = readVar(part[1]);
                                    part[2] = "=";
                                    ifValue = int.Parse(part[3]);




                                    if (ifVar == ifValue)
                                    {
                                        executeLinesFlag = true;
                                        MessageBox.Show("Running through the if");

                                    }
                                    else
                                    {

                                        executeLinesFlag = false;
                                        MessageBox.Show("Skipping the if");

                                        count = programLine.Count;

                                    }


                                    break;

                                case "endif":
                                   
                                    executeLinesFlag = true;

                                    break;

                            }//end of switch
                        }
                    }
                    catch (Exception) {

                       
                    }


                textBox1.ResetText();//resets the text within the textbox after each enter press to make it easier to use
                }

            }

        }

        
    }
    }

    




