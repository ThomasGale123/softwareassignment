using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareAssignment
{
    /// <summary>
    /// Interface of overrideable methods to be used within the shape classes.
    /// An interface is need for us to use our factory within the form.
    /// </summary>
    interface ShapeInterface
    {
        void setShape(Color colour, params int[] list);

        void setTriangle(Color colour, int x,int y, Point[] sides); 
        void drawShape(Graphics g);

    }
}
