using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareAssignment
{
    /// <summary>
    /// The shape class is the base class that all the other shapes inherit from.
    /// No need for repeating code as all shapes use x,y for positioning
    /// We also have a ShapeInterface which allows for the factory and the Shape classes to override the drawing methods
    /// </summary>
    public abstract class Shape : ShapeInterface
    {
        protected Color colour;
        protected int x, y;
        Point[] sides = new Point[3];

        public Shape()
        {
            colour = Color.Green;
            x = y = 0;
        }
        //this is the base Shape that all other shapes will implement
        protected Shape(Color colour,int x, int y)
        {
            this.colour = colour;
            this.x = x;
            this.y = y;
        }
        //The setShape,drawShape,toString are declared here as they will be used within each class
        public virtual void setShape(Color colour, params int[] list)
        {
            this.colour = colour;
            this.x = list[0];
            this.y = list[1];
        }

        public virtual void setTriangle(Color colour, int x, int y, Point[] sides)
        {
            this.colour = colour;
            this.x = x;
            this.y = y;
        }

        public abstract void drawShape(Graphics g);

        public override string ToString()
        {
            return base.ToString() + "    " + this.x + "," + this.y + " : ";
        }
    }
}
