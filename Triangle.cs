﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareAssignment
{
    /// <summary>
    /// The triangle inherits the x,y but also has Points which are unique to the triangle
    /// </summary>
    public class Triangle : Shape
    {
        Point[] sides = new Point[3];

        public Triangle()
        {
        }

        public Triangle(Point[] sides)
        {
            this.sides = sides;
        }
        //The structure in which the triangle will be created within the form
        public Triangle(Color colour,int x, int y, Point[] sides) : base(colour,x, y)
        {
            this.sides = sides;
        }

        public override void setTriangle(Color colour, int x, int y, Point[] sides)
        {
            //colour,x,y are all inherited from the base class and width list[2] height list [3] are unique to rectangle
            base.setShape(colour, x, y);
            this.sides = sides;
            

        }

        //A pen and brush are declared specific for the triangle when this is called and disposed of once drawn
        public override void drawShape(Graphics g)
        {
            Pen p = new Pen(Color.Black, 2);
            Brush b = new SolidBrush(colour);
            g.DrawPolygon(p, sides);
            g.FillPolygon(b, sides);
            p.Dispose();
            b.Dispose();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
