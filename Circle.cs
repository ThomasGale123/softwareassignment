﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareAssignment
{
    /// <summary>
    /// The circle class inherits x,y as shown in the base but also has a radius which is unique to the circle
    /// </summary>
    public class Circle : Shape
    {
        int radius;

        public Circle():base()
        {

        }

        public Circle(int radius)
        {
            this.radius = radius;
        }
        //The structure in which the circle will be created within the form
        public Circle(Color colour,int x, int y, int radius) : base(colour,x, y)
        {
            this.radius = radius;
        }

        public Circle(int x, int y, int radius)
        {
            this.x = x;
            this.y = y;
            this.radius = radius;
        }

        public override void setShape(Color colour, params int[] list)
        {
            //colour,x,y are all inherited from the base class and radius is the only unique value to circle so this is declared in list[2]
            base.setShape(colour, list[0], list[1]);
            this.radius = list[2];

        }
        //A pen and brush are declared specific for the circle when this is called and disposed of once drawn
        public override void drawShape(Graphics g)
        {
            Pen p = new Pen(Color.Black, 2);
            Brush b = new SolidBrush(colour);
            g.FillEllipse(b, x, y, radius * 2, radius * 2);
            g.DrawEllipse(p, x, y, radius * 2, radius * 2);
            p.Dispose();
            b.Dispose();
        }

        public override string ToString()
        {
            return base.ToString() + "  " + this.radius;
        }
    }
}
