using SoftwareAssignment;
using System;
using System.Collections;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SoftwareAssignment.Tests
{
    [TestClass()]
    public class UnitTest1
    {
        /// <summary>
        /// This test is checking if i am able to create a circle using specified values.
        /// The x,y are inherited from shape and radius is unique to shape so this shows that the inheritance aspect is
        /// functioning correctly.
        ///The Circle is added to an array to demonstrate that it exists.
        ///In a functional situation the values will be parsed from the input of the textbox/richtextbox to get the values.
        /// </summary>
        [TestMethod()]
        public void CircleTest()
        {
            ArrayList shapes = new ArrayList();

            int x = 100;
            int y = 100;
            int radius = 100;
            shapes.Add(new Circle(x, y, radius));

            if (shapes.Count > 0)
            {

                Console.WriteLine("A shape exists" + shapes.Count);
            }
            else
            {
                Console.WriteLine("No shape exists");
                Assert.Fail();

            }
        }
        /// <summary>
        /// This unit test is counting if the line has something withinn then adding it to an array
        /// from the array the lines would then be counted through one by one through a switch.
        /// </summary>
        [TestMethod()]
        public void commandParserTest()
        {
            ArrayList programLine = new ArrayList();
            String[] lines = { "circle 100", "circle 200", "circle 300", "circle 100", "circle 200", "circle 300" };
            String[] line;
            int i = 0;
            while (lines.Length != i)
            {
                line = lines[i].Split(',');
                programLine.Add(line);
                i++;
            }
            System.Console.WriteLine("the number of lines are" + programLine.Count.ToString());
        }

        /// <summary>
        /// This is one of the paramater checking methods i use for each case within my switch that needs to parse an int
        /// if the values within the line are not equal to an integer then a message is thrown instead telling the user
        /// that it must contain a number. If a number is displayed then the values will be added to the relative command
        /// with an else statement
        /// </summary>
        [TestMethod]
        public void numParamCheck()
        {
            int x, y;
            if (!int.TryParse("0", out x) || !int.TryParse("0", out y))
            {
                System.Console.WriteLine("the line is invalid");

            }
            else
            {
                System.Console.WriteLine("the line is valid");
            }
        }

        /// <summary>
        /// The null paramater check works similar to the num paramater check in the way that if the place in the line
        /// where the value should be is equal to "" then it will return an error that the value cannot be null.
        /// </summary>
        [TestMethod]
        public void nullParamCheck()
        {
            String nullCheck = "";
            String test = "";
            if (nullCheck == test)
            {
                System.Console.WriteLine("the line is null");

            }
            else
            {
                System.Console.WriteLine("the line is not null");
            }
        }

        /// <summary>
        /// The invalid command method works by matching what is entered to a command already existing within the switch
        /// if the case exists then the case is passed else it will default to an invalid command.
        /// </summary>
        [TestMethod]
        public void invalidCommand()
        {

            String rectangleCom = "rectangle";

            if (rectangleCom == "circle")
            {
                System.Console.WriteLine("The shape is the same");

            }
            else
            {
                System.Console.WriteLine("The shape does not match");


            }
        }

        /// <summary>
        /// This if test is checking if the value on the left is equal to the one on the right. If they are equal then the lines are executed
        /// else if they are not it will return that the values do not match and skip over the next line like it would within the program.
        /// </summary>
        [TestMethod]
        public void ifCommand()

        {
            int ifVar = 0;
            int ifValue = 1;
            string condition = "=";
            bool executeLinesFlag = true;
            int count = 0;

            if (condition == "=")
            {

                if (ifVar == ifValue)
                {
                    executeLinesFlag = true;
                    System.Console.WriteLine("Executing lines");
                }
                else
                {
                    executeLinesFlag = false;
                    System.Console.WriteLine("The variable values does not match");
                    count++;
                }
            }
        }

        /// <summary>
        /// This method is used to check if the variable inputted exists within VariableNames by comparing the variable we are looking for
        /// against every listing within the array using the for loop until it is not able to count anymore. If no such command is
        /// returned then it does not exist else it does and an error will be thrown to the user saying that the variable has already
        /// been declared
        /// </summary>
        [TestMethod]
        public void checkVariable()
        {
            string[] variableNames = { "James" };
            int variableCounter = 0;
            
            for (int loop = 0; loop < variableCounter; loop++)
            {
                if (variableNames[loop] == "James")
                {
                    System.Console.WriteLine("I am here");
                }
            }
            System.Console.WriteLine("I am not here");
            
        }

     

        /// <summary>
        /// This method is for generating random values for use within my factory. All shapes share these values so i thought it would
        /// makes sense to create a method to call these values from when they are needed. This makes for cleaner looking code.
        /// </summary>
        [TestMethod]
        public void randomGen()
        {
            Random rand = new Random(77887);
            int x, y;
            x = rand.Next(50,500);
            y = rand.Next(50,500);

            int red = rand.Next(255);
            int green = rand.Next(255);
            int blue = rand.Next(255);

            System.Console.WriteLine("my" + x + "my" + y + "my colour" + red + green + blue);

        }





    }
}



        

    


